package com.cloudsherpas.spring.cloudendpoint.config;

import com.google.api.server.spi.SystemServiceServlet;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

/**
 * Configuration class to inject current context to Spring Framework servlet
 */
public class AutowireSupportServiceServlet extends SystemServiceServlet {
    @Override
    protected <T> T createService(Class<T> arg0) {
        T service = super.createService(arg0);
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(service);
        return service;
    }
}
