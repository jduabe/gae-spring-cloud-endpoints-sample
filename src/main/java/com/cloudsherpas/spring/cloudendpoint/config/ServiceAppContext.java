package com.cloudsherpas.spring.cloudendpoint.config;

import com.cloudsherpas.spring.cloudendpoint.service.SampleService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

/**
 * Annotation based service configuration for Spring autowiring
 */
@Configuration
@Lazy
public class ServiceAppContext {

    @Bean(name = "sampleService")
    public SampleService getSampleService() {
        return new SampleService();
    }
}
