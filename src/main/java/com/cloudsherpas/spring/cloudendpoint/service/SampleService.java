package com.cloudsherpas.spring.cloudendpoint.service;

import com.cloudsherpas.spring.cloudendpoint.dto.SampleDTO;

/**
 * Sample service which returns a pre-defined response to the API
 */
public class SampleService {

    private static final String RESPONSE_MESSAGE = "Hello, World!";

    /**
     * Return a sample response
     *
     * @return String response message
     */
    public SampleDTO getSampleResponse() {
        final SampleDTO sampleDTO = new SampleDTO();
        sampleDTO.setMessage(RESPONSE_MESSAGE);

        return sampleDTO;
    }
}
