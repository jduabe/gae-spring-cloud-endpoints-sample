package com.cloudsherpas.spring.cloudendpoint.api;

import com.cloudsherpas.spring.cloudendpoint.dto.SampleDTO;
import com.cloudsherpas.spring.cloudendpoint.service.SampleService;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;

/**
 * Sample API using Google Cloud Endpoints
 */
@Api(
        name = "sample",
        version = "v1",
        description = "Sample API"
)
public class SampleResource {

    @Autowired
    @Qualifier("sampleService")
    @Lazy
    private SampleService sampleService;

    /**
     * Sample GET endpoint using Google Cloud Endpoints
     *
     * @return Pre-defined sample response
     */
    @ApiMethod(
            name = "response.get",
            path = "response",
            httpMethod = ApiMethod.HttpMethod.GET)
    public SampleDTO getSampleResponse() {
        return sampleService.getSampleResponse();
    }
}
