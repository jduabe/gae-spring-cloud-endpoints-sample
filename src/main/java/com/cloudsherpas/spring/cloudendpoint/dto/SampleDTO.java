package com.cloudsherpas.spring.cloudendpoint.dto;

/**
 * Sample bean
 */
public class SampleDTO {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
