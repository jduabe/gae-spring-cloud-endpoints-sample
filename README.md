GAE + Google Cloud Endpoints + Spring Framework Sample
==================

A sample application using Google Cloud Endpoints for APIs and Spring Framework
(spring-core) for dependency injection written in Java.

## Running the application

Execute the following Maven goal on the command line:

```
#!java
mvn appengine:devserver
```
